<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="ucapan.html" method="POST">
        <label for="firstname">First Name:</label><br>
        <input type="text" name="firstname" id="firstname" required><br><br>

        <label for="lastname">Last Name:</label><br>
        <input type="text" name="lastname" id="lastname" required><br><br>

        <label for="gender">Gender:</label><br>
            <input type="radio" name="gender" required id="gender"> laki-laki
            <input type="radio" name="gender" required id="gender"> perempuan
            <input type="radio" name="gender" required id="gender"> other
        <br><br>

        <label for="nationality">Nationality:</label><br>
        <select id="nationality" required>
            <option value="indonesian" name="nationality">Indonesian</option>
            <option value="american" name="nationality">American</option>
            <option value="german" name="nationality">German</option>
        </select>
        <br><br>

        <label for="languagespoken">Language Spoken: </label><br>
        <input type="checkbox" name="languagespoken" id="languagespoken" value="ID">Indonesian <br>
        <input type="checkbox" name="languagespoken" id="languagespoken" value="ENG">English <br>
        <input type="checkbox" name="languagespoken" id="languagespoken" value="OT" required>Other <input type="text"> <br>
        <br><br>

        <label for="biografi">Bio:</label><br>
          <textarea class="form-control" name="biografi" id="biografi" rows="7"></textarea>
        </div>
        <br>

        <input type="submit">
    </form>
</body>
</html>